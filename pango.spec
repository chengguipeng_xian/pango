Name:           pango
Version:        1.45.3
Release:        1
Summary:        A library for layout and rendering of text

License:        LGPLv2+
URL:            https://pango.gnome.org/
Source0:        https://download.gnome.org//sources/pango/1.45/%{name}-%{version}.tar.xz

Patch9000:	disable-layout-test.patch

BuildRequires:  pkgconfig(cairo) >= 1.12.10 pkgconfig(fontconfig) >= 2.12.92 pkgconfig(freetype2) >= 2.1.5
BuildRequires:  pkgconfig(fribidi) >= 1.0 pkgconfig(glib-2.0) >= 2.59.2 pkgconfig(harfbuzz) >= 2.0.0 
BuildRequires:  pkgconfig(xft) >= 2.0.0 pkgconfig(libthai) >= 0.1.9 pkgconfig(gobject-introspection-1.0) 
BuildRequires:  cairo-gobject-devel gtk-doc meson help2man gcc gcc-c++ harfbuzz-help 

Requires:       cairo >= 1.12.10 fontconfig >= 2.12.92 freetype >= 2.1.5 fribidi >= 1.0
Requires:       glib2 >= 2.59.2 harfbuzz >= 2.0.0 libXft >= 2.0.0 libthai >= 0.1.9

%description
Pango is a library for layout and rendering of text, with an emphasis
on internationalization. Pango can be used anywhere that text layout
is needed; however, most of the work on Pango so far has been done using 
the GTK+ widget toolkit as a test platform. Pango forms the core of text
and font handling for GTK+-2.x.

%package        devel
Summary:        Development environment for %{name}
Requires:       %{name} = %{version}-%{release} freetype-devel >= 2.1.5
Requires:       glib2-devel >= 2.59.2 fontconfig-devel >= 2.12.92 cairo-devel >= 1.12.10
Provides:       %{name}-tests = %{version}-%{release}
Obsoletes:      %{name}-tests < %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries header files and tests for
the %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson -Denable_docs=true  -Dinstall-tests=true
%meson_build

%install
%meson_install

test -e %{buildroot}%{_libdir}/libpangoxft-1.0.so

%check
%meson_test

%files
%defattr(-,root,root)
%doc README.md
%license COPYING
%{_libdir}/libpango*-*.so.*
%{_bindir}/%{name}-list
%{_bindir}/%{name}-view
%{_libdir}/girepository-1.0/Pango*-1.0.typelib

%files devel
%defattr(-,root,root)
%{_libdir}/*.so
%{_includedir}/*
%{_libdir}/pkgconfig/*
%{_datadir}/gir-1.0/*.gir
%{_libexecdir}/installed-tests/pango/
%{_datadir}/installed-tests

%files help
%defattr(-,root,root)
%doc NEWS 
%{_mandir}/man1/pango-view.1.*

%changelog
* Mon Jul 20 2020 wangye <wangye70@huawei.com> -1.45.3-1
- Type:bugfix
- Id:NA
- SUG:NA
- Mainline branch update to 1.44.7

* Mon Jun 15 2020 hanhui <hanhui15@huawei.com> -1.44.7-1
- Type:bugfix
- Id:NA
- SUG:NA
- Mainline branch update to 1.44.7

* Thu Mar 19 2020 hexiujun<hexiujun1@huawei.com> - 1.43.0-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:enable test

* Sat Nov 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.43.0-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add the libxslt in buildrequires

* Wed Sep 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.43.0-2
- Type:cves
- ID:CVE-2019-1010238
- SUG:NA
- DESC:fix CVE-2019-1010238

* Sun Sep 15 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.43.0-1
- Package Init

